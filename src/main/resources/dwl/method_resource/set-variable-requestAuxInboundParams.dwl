%dw 2.0
output application/json
---
{	
	(headers:{
	    ("Client-Id": attributes.headers.'Client-Id') if (!isEmpty(attributes.headers.'Client-Id')),
	    ("Authorization": attributes.headers.'Authorization') if (!isEmpty(attributes.headers.'Authorization')),
	    "X-Correlation-Id": vars.correlationId default correlationId,
	    ("Content-Type": attributes.headers.'Content-Type') if (!isEmpty(attributes.headers.'Content-Type')),
	    ("X-Transaction-Id": vars.xTransactionId) if (!isEmpty(vars.xTransactionId)),
		("User-Name" : attributes.headers.'User-Name') if (!isEmpty(attributes.headers.'User-Name')),
		("User-Password" : attributes.headers.'User-Password') if (!isEmpty(attributes.headers.'User-Password'))
	})if (!isEmpty(attributes.headers)),
	("queryParams": attributes.queryParams) if (!isEmpty(attributes.queryParams))
	
}