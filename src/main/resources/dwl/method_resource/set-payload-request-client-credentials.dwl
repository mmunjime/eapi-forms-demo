output application/x-www-form-urlencoded
---
{
    grant_type: "client_credentials",
    scope: p('oauth.scope'),
    client_id: p('oauth.client_id'),
    client_secret: p('oauth.client_secret')
}