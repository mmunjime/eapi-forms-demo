{
  "headers": {
    "x-correlation-id": "31c451d5-3ea9-46fc-9927-bc073fd97a9a",
    "user-agent": "PostmanRuntime/7.28.4",
    "accept": "*/*",
    "cache-control": "no-cache",
    "postman-token": "94b1d6dd-c126-4cdf-9724-bf48abe689b9",
    "host": "localhost:8092",
    "accept-encoding": "gzip, deflate, br",
    "connection": "keep-alive"
  },
  "clientCertificate": null,
  "method": "GET",
  "scheme": "https",
  "queryParams": {
    "mode": "verbose"
  },
  "requestUri": "/v1/ping?mode=verbose",
  "queryString": "mode=verbose",
  "version": "HTTP/1.1",
  "maskedRequestPath": "/ping",
  "listenerPath": "/v1/*",
  "relativePath": "/v1/ping",
  "localAddress": "/127.0.0.1:8092",
  "uriParams": {},
  "rawRequestUri": "/v1/ping?mode=verbose",
  "rawRequestPath": "/v1/ping",
  "remoteAddress": "/127.0.0.1:53448",
  "requestPath": "/v1/ping"
}